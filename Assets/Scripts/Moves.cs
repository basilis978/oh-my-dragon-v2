﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Moves : MonoBehaviour
{
 
    public Button _swordBtn;
    public Button _shieldBtn;
    public Animator _player;

    public GameObject _hit;
    public GameObject _block;
    public GameObject _walk;
    public GameObject _idle;
    public GameObject _freeze;
    public GameObject _freezew;

   // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        _swordBtn.onClick.AddListener(Hit);
        _shieldBtn.onClick.AddListener(Block);

    }
    public void Hit()
    {  
            _hit.SetActive(false);

    }

    public void Block()
    {
            _block.SetActive(false);

    }

    public void Freeze()
    {

            _freeze.SetActive(false);
    }
    public void Walk()
    {

            _walk.SetActive(false);

    }
}
