﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class IceBolt : MonoBehaviour
{
    // Either drag in via Inspector
    [SerializeField] public GameObject _IceBolt;
    [SerializeField] public Transform _Startingpoint;
    private GameObject publicSpit;

    public void Start()
    {
        
    }
    // and now call this from the AnimationEvent
    public void DoIt()
    {
        publicSpit = IceSpit();
        Destroy(publicSpit, 6);
    }

    public GameObject IceSpit()
    {
        GameObject _IceSpit = Instantiate(_IceBolt, _Startingpoint.transform.position, Quaternion.identity);
        _IceSpit.SetActive(true);
        return _IceSpit;
    }
}

