﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.PlayerLoop;
using UnityEngine.XR.WSA;

public class Projectile : MonoBehaviour
{
    [SerializeField] public GameObject PlayerPoint;
    [SerializeField] public GameObject DragonPoint;
    [SerializeField] public float speed = 0.08f;
    private Vector3 point;
    private IceBolt _Icebolt;
    public Transform target;
    void Start()
    {
        point = PlayerPoint.transform.position;

    }
    void Update()
    {
        float step = speed * Time.deltaTime;
        //transform.Translate( point * step, Space.Self);
        transform.position = Vector3.MoveTowards(transform.position, PlayerPoint.transform.position, step);
        // = Transform.LookAt(target.transform.position);
    /*    Vector2 direction = target.position - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, speed * Time.deltaTime);*/
    }

}
