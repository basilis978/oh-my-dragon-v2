﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameObject player;
    private Rigidbody2D _rigid;
    private Vector2 movement;

    void Start()
    {   
        Player player = GetComponentInChildren<Player>();
        _rigid = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
       
        //horizontal input for left/right
        float horizontalInput = Input.GetAxisRaw("Horizontal");
            if (horizontalInput < 0.0f)
            {
                player.transform.localScale = new Vector3(-1f,1f,1f);
            }
            else
            {
                player.transform.localScale = new Vector3(1f,1f,1f);
            }

        //current velocity =  new velocity(x,currentvelocity.y);
        _rigid.velocity = new Vector2(horizontalInput*2.5f,_rigid.velocity.y);

    }
}
