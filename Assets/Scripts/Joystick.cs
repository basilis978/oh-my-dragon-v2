﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Joystick : MonoBehaviour {

    public Animator anim;
    public GameObject pl;
    public SpriteRenderer sr;
    public Button swordBtn;
    public Button shieldBtn;

    private Vector2 pointA;
    private Vector2 pointB;
    private Vector2 gravity;

    public Transform player;
    public Transform circle;
    public Transform outerCircle;

    public float speed = 5.0f;
    private bool touchStart = false;

    void Start() 
    {
        gravity = new Vector2(1f, 0.0f);
        Button btn = swordBtn.GetComponent<Button>();
        btn.onClick.AddListener(Hit);
        Button btn1 = shieldBtn.GetComponent<Button>();
        btn1.onClick.AddListener(Block);
        
    }
    void Hit()
    {
        anim.SetBool("IsHitting",true);
    }
    void Block()
    {
        anim.SetBool("IsBlocking",true);
    }
    
	// Update is called once per frame
	void Update () 
    {
        Button btn = swordBtn.GetComponent<Button>();
        btn.onClick.AddListener(Hit);
        Button btn1 = shieldBtn.GetComponent<Button>();
        btn1.onClick.AddListener(Block);

        if(Input.GetMouseButtonDown(0))
        {
            pointA = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.z));

            circle.transform.position = pointA ;
            outerCircle.transform.position = pointA ;
            circle.GetComponent<SpriteRenderer>().enabled = true;
            outerCircle.GetComponent<SpriteRenderer>().enabled = true;
        }
        if(Input.GetMouseButton(0))
        {
            touchStart = true;
            pointB = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.z));
        }
        else
        {
            touchStart = false;
        }

        
	}
	private void FixedUpdate()
    {
        if(touchStart)
        {
            Vector2 offset = pointB - pointA;
            if (pointB.x > pointA.x)
            {
                sr.flipX = false;
                pl.transform.localScale = new Vector3(1f * pl.transform.localScale.x, 1f * pl.transform.localScale.y, 1f * pl.transform.localScale.z);
                Debug.Log("Turn left");
            }
            else
            {
                sr.flipX = true;
                pl.transform.localScale = new Vector3(1f * pl.transform.localScale.x, 1f* pl.transform.localScale.y, 1f* pl.transform.localScale.z);

                Debug.Log("Turn right");
            }

            Vector2 renderange = new Vector2(0.1f,0.1f);
            Vector2 direction = Vector2.ClampMagnitude(offset, 1.0f);
            moveCharacter(direction);
            
            anim.SetBool("IsWalking",true);

            circle.transform.position = new Vector2(pointA.x + direction.x, pointA.y + direction.y);
        }
        else
        {
            circle.GetComponent<SpriteRenderer>().enabled = false;
            outerCircle.GetComponent<SpriteRenderer>().enabled = false;
            anim.SetBool("IsWalking",false);
        }

	}
	void moveCharacter(Vector2 direction)
    {
        player.Translate(direction * gravity * speed * Time.deltaTime);
    }

}