﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Equip : MonoBehaviour
{
    public GameObject swordBtn;
    public GameObject shieldBtn;

    public GameObject sword;
    public GameObject shield;
    public GameObject bothEquippables;
    public GameObject noWeapons;

    public void EnableSword()
    {
        if(!sword.activeInHierarchy)
        {   
            noWeapons.SetActive(false);
            sword.SetActive(true);
            swordBtn.SetActive(false);
        }
        EnableBoth();
    }

    public void EnableShield()
    {
        if(!shield.activeInHierarchy)
        {
            noWeapons.SetActive(false);
            shield.SetActive(true);
            shieldBtn.SetActive(false);
        }
        EnableBoth();
    }

    public void EnableBoth()
    {
        if(shield.activeInHierarchy && sword.activeInHierarchy)
        {
            shield.SetActive(false);
            sword.SetActive(false);
            bothEquippables.SetActive(true);
        }
    }
}
