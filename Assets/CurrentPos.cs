﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentPos : MonoBehaviour
{
    private Transform pos;

    // Start is called before the first frame update
    void Start()
    {
        pos = GameObject.Find("PlayerPoint").transform;
    }

    public Transform PosTransfer()
    {
        return pos;
    }
}
